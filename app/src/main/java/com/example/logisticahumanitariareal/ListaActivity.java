package com.example.logisticahumanitariareal;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import database.AgendaEstados;
import database.Estado;
import database.ProcesosPHP;

public class ListaActivity extends ListActivity {


    private AgendaEstados agendaEstados;
    private Button btnNuevo;
    ProcesosPHP php;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        this.php=new ProcesosPHP();
        php.setContext(this);
        this.btnNuevo = (Button)findViewById(R.id.btnNuevo);
        this.agendaEstados = new AgendaEstados(this);
        this.agendaEstados.openDatabase();
        ArrayList<Estado> contactos = agendaEstados.allContactos();
        MyArrayAdapter adapter = new MyArrayAdapter(this,R.layout.activity_estado, contactos);
        setListAdapter(adapter);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(ListaActivity.this,MainActivity.class);
                i.putExtra("id",0);
                startActivity(i);
                finish();
            }
        });
    }
    class MyArrayAdapter extends ArrayAdapter<Estado> {
        Context context;
        int textViewResourceId;
        ArrayList<Estado> objects;
        public MyArrayAdapter(Context context, int textViewResourceId,ArrayList<Estado> objects){
            super(context, textViewResourceId, objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }
        public View getView(final int position, View convertView, ViewGroup viewGroup){
            LayoutInflater layoutInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, null);
            TextView lblNombre = (TextView)view.findViewById(R.id.lblNombreEstado);
            Button modificar = (Button)view.findViewById(R.id.btnModificar);
            Button borrar = (Button)view.findViewById(R.id.btnBorrar);
            lblNombre.setText(objects.get(position).getEstado());
            borrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new AlertDialog.Builder(context)
                            .setTitle("Confirmar")
                            .setMessage("¿Deseas Modificarlo?")
                            .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    php.borrarContactoWebService(String.valueOf(objects.get(position).get_ID()));
                                    agendaEstados.openDatabase();
                                    agendaEstados.deleteContacto(objects.get(position).get_ID());
                                    agendaEstados.close();
                                    objects.remove(position);
                                    notifyDataSetChanged();
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                }
                            }).show();
                }
            });
            modificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("estado",objects.get(position));
                    Intent i = new Intent();
                    i.putExtras(bundle);
                    setResult(Activity.RESULT_OK,i);
                    finish();
                }
            });
            return view;
        }
    }

}
