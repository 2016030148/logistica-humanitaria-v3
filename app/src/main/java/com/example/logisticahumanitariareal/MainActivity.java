package com.example.logisticahumanitariareal;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import database.AgendaEstados;
import database.Device;
import database.Estado;
import database.ProcesosPHP;

public class MainActivity extends AppCompatActivity implements Response.Listener<JSONObject>,Response.ErrorListener{
    private Button btnGuardar;
    private Button btnListar;
    private Button btnLimpiar;
    private EditText txtEstado;
    private Estado savedEstado;
    private int id;
    ProcesosPHP php;
    AgendaEstados agenda;
    JsonObjectRequest jsonObjectRequest;
    private RequestQueue request;
    private String serverip = "https://josuedanielsosavaldez.000webhostapp.com/WebService/";
    ArrayList<Estado> estados = new ArrayList<Estado>();
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.php=new ProcesosPHP();
        php.setContext(this);
        this.request = Volley.newRequestQueue(this);
        this.agenda=new AgendaEstados(this);
        setContentView(R.layout.activity_main);
        btnGuardar = (Button) findViewById(R.id.btnGuardar);
        btnListar = (Button) findViewById(R.id.btnListar);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        txtEstado = (EditText) findViewById(R.id.txtEstado);
        sharedPreferences = getSharedPreferences("datos", Context.MODE_PRIVATE);
        boolean agregados = sharedPreferences.getBoolean("agregados",false);
        if(!agregados) {
            if(isNetworkAvailable()) {
                this.mostrarTodosWebService(Device.getSecureId(MainActivity.this));
            }
        }

        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    boolean completo = true;
                    if (txtEstado.getText().toString().equals("")) {
                        txtEstado.setError("Introduce el Nombre");
                        completo = false;
                    }
                    if (completo) {
                        final Estado nEstado = new Estado();
                        nEstado.setEstado(txtEstado.getText().toString());
                        nEstado.setIdmovil(Device.getSecureId(MainActivity.this));
                        if (savedEstado == null) {
                            php.insertarContactoWebService(nEstado);
                            agenda.openDatabase();
                            agenda.insertContacto(nEstado);
                            agenda.close();
                            Toast.makeText(getApplicationContext(), "Agregado", Toast.LENGTH_SHORT).show();
                            limpiar();
                        } else {
                            if (savedEstado.getEstado().equals(txtEstado.getText().toString())) {
                                Intent i = new Intent(MainActivity.this, ListaActivity.class);
                                startActivityForResult(i, 0);
                            } else {
                                new AlertDialog.Builder(MainActivity.this)
                                        .setTitle("Confirmar")
                                        .setMessage("¿Deseas Modificarlo?")
                                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int whichButton) {
                                                php.actualizarContactoWebService(nEstado, id);
                                                agenda.openDatabase();
                                                agenda.updateContacto(nEstado, id);
                                                agenda.close();
                                                limpiar();
                                                Toast.makeText(getApplicationContext(), "Modificado", Toast.LENGTH_SHORT).show();
                                                Intent i = new Intent(MainActivity.this, ListaActivity.class);
                                                startActivityForResult(i, 0);
                                            }
                                        })
                                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int whichButton) {
                                                Intent i = new Intent(MainActivity.this, ListaActivity.class);
                                                startActivityForResult(i, 0);
                                            }
                                        }).show();
                            }
                        }

                    }
                }
                else
                    Toast.makeText(MainActivity.this, "No  hay conexión", Toast.LENGTH_SHORT).show();
            }
        });

        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, ListaActivity.class);
                startActivityForResult(i, 0);
                limpiar();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limpiar();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (Activity.RESULT_OK == resultCode){
            Estado estado = (Estado) data.getSerializableExtra("estado");
            savedEstado = estado;
            id = estado.get_ID();
            txtEstado.setText(estado.getEstado());
        }
    }

    public void limpiar(){
        txtEstado.setText("");

        txtEstado.setError(null);
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }
    public void mostrarTodosWebService(String idMovil){
        String url = serverip + "wsConsultarTodos.php?idMovil="+idMovil;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {}

    @Override
    public void onResponse(JSONObject response) {
        JSONArray json = response.optJSONArray("estados");
        try {
            AgendaEstados agenda = new AgendaEstados(MainActivity.this);
            agenda.openDatabase();
            Estado estado;
            if(json==null)
                return;
            for (int i = 0;i < json.length(); i++) {
                estado = new Estado();
                JSONObject jsonObject = null;
                jsonObject = json.getJSONObject(i);
                estado.set_ID(jsonObject.optInt("_ID"));
                estado.setEstado(jsonObject.optString("estado"));
                estado.setIdmovil(jsonObject.optString("idMovil"));
                agenda.insertContacto(estado);
            }
            agenda.close();
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean("agregados", true);
            editor.commit();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

