package database;

import java.io.Serializable;

public class Estado implements Serializable {

    private int _ID;
    private String estado;
    private String idmovil;

    public Estado(){
        this._ID = 0;
        this.estado = "";
        this.idmovil = "";
    }
    public Estado(int _ID,String estado,String idmovil){
        this._ID = _ID;
        this.estado = estado;
        this.idmovil = idmovil;
    }

    public int get_ID() {
        return _ID;
    }

    public void set_ID(int _ID) {
        this._ID = _ID;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getIdmovil() {
        return idmovil;
    }

    public void setIdmovil(String idmovil) {
        this.idmovil = idmovil;
    }
}
