package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

public class AgendaEstados {

    Context context;
    EstadoDbHelper myDbHelper;
    SQLiteDatabase db;
    String[] columnas = new String[]{
            DefinirTabla.Estado._ID,
            DefinirTabla.Estado.COLUMN_NAME_ESTADO,
            DefinirTabla.Estado.COLUMN_NAME_IDMOVIL
    };

    public AgendaEstados(Context context){
        this.context = context;
        this.myDbHelper = new EstadoDbHelper(this.context);
    }

    public void openDatabase(){
        db = myDbHelper.getWritableDatabase();
    }

    public long insertContacto(Estado c){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Estado.COLUMN_NAME_ESTADO, c.getEstado());
        values.put(DefinirTabla.Estado.COLUMN_NAME_IDMOVIL, c.getIdmovil());
        return db.insert(DefinirTabla.Estado.TABLE_NAME, null, values);
        //regresa el id insertado
    }

    public long updateContacto(Estado c,int id){
        ContentValues values = new ContentValues();
        values.put(DefinirTabla.Estado.COLUMN_NAME_ESTADO, c.getEstado());
        values.put(DefinirTabla.Estado.COLUMN_NAME_IDMOVIL, c.getIdmovil());
        Log.d("values", values.toString()); //numero de filas afectadas
        return db.update(DefinirTabla.Estado.TABLE_NAME , values,
                DefinirTabla.Estado._ID + " = " + id,null);
    }

    public int deleteContacto(long id){
        return db.delete(DefinirTabla.Estado.TABLE_NAME,DefinirTabla.Estado._ID + "=?",
                new String[]{ String.valueOf(id) });
    }

    private Estado readContacto(Cursor cursor){
        Estado c = new Estado();
        c.set_ID(cursor.getInt(0));
        c.setEstado(cursor.getString(1));
        c.setIdmovil(cursor.getString(2));
        return c;
    }

    public void close(){
        myDbHelper.close();
    }

    public Estado getContacto(long id){
        SQLiteDatabase db = myDbHelper.getReadableDatabase();
        Cursor c = db.query(DefinirTabla.Estado.TABLE_NAME,
                columnas,
                DefinirTabla.Estado._ID + " = ?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null
        );
        c.moveToFirst();
        Estado contacto = readContacto(c);
        c.close();
        return contacto;
    }

    public ArrayList<Estado> allContactos(){
        Cursor cursor = db.query(DefinirTabla.Estado.TABLE_NAME,
                columnas, null, null, null, null, null);
        ArrayList<Estado> contactos = new ArrayList<Estado>();
        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            Estado c = readContacto(cursor);
            contactos.add(c);
            cursor.moveToNext();
        }
        cursor.close();
        return contactos;
    }

}
