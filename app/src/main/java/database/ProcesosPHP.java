package database;
import android.content.Context;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class ProcesosPHP implements Response.Listener<JSONObject>,Response.ErrorListener{
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Estado> contactos = new ArrayList<Estado>();
    private boolean consulta;
    private String serverip = "https://josuedanielsosavaldez.000webhostapp.com/WebService/";

    public void setContext(Context context){
        request = Volley.newRequestQueue(context);
    }
    public void insertarContactoWebService(Estado e){
        this.consulta=false;
        String url = serverip + "wsRegistro.php?estado="+e.getEstado()+"&idMovil="+e.getIdmovil();
        url = url.replace(" ","%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }
    public void actualizarContactoWebService(Estado e,int id){
        this.consulta=false;
        String url = serverip + "wsActualizar.php?_ID="+id+"&estado="+e.getEstado()+"&idMovil="+e.getIdmovil();
        url = url.replace(" ","%20");
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    public void borrarContactoWebService(String id){
        this.consulta=false;
        String url = serverip + "wsEliminar.php?_ID="+id;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        request.add(jsonObjectRequest);
    }

    public ArrayList<Estado> mostrarTodosWebService(String idMovil)
    {
        this.consulta=true;
        String url = serverip + "wsConsultarTodos.php?idMovil="+idMovil;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
        return this.contactos;
    }

    public ArrayList<Estado> mostrarContactoWebService(int idMovil)
    {
        this.consulta=true;
        String url = serverip + "wsConsultarContacto.php?idMovil="+idMovil;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this, this);
        request.add(jsonObjectRequest);
        return this.contactos;
    }
    @Override
    public void onResponse(JSONObject response) {
        if(this.consulta) {
            this.contactos.removeAll(this.contactos);
            Estado contacto = null;
            JSONArray json = response.optJSONArray("estados");
            try {
                for (int i = 0;json!=null && i < json.length(); i++) {
                    contacto = new Estado();
                    JSONObject jsonObject = null;
                    jsonObject = json.getJSONObject(i);
                    contacto.set_ID(jsonObject.optInt("_ID"));
                    contacto.setEstado(jsonObject.optString("estado"));
                    contacto.setIdmovil(jsonObject.optString("idMovil"));
                    contactos.add(contacto);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
}
