package database;

import android.provider.BaseColumns;

public class DefinirTabla {
    public static abstract class Estado implements BaseColumns
    {
        public static final String TABLE_NAME = "Estados";
        public static final String COLUMN_NAME_ESTADO = "nombre";
        public static final String COLUMN_NAME_IDMOVIL = "idmovil";
    }
}
