package database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class EstadoDbHelper extends SQLiteOpenHelper {

    private static final String INTEGER_TYPE = " INTEGER";
    private static final String PRIMARY_KEY = " PRIMARY KEY";
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMA = ", ";
    private static final String SQL_CREATE_CONTACTO =
            "CREATE TABLE " + DefinirTabla.Estado.TABLE_NAME +
                    " ("+DefinirTabla.Estado._ID + INTEGER_TYPE + PRIMARY_KEY + COMA +
                    DefinirTabla.Estado.COLUMN_NAME_ESTADO + TEXT_TYPE + COMA +
                    DefinirTabla.Estado.COLUMN_NAME_IDMOVIL + TEXT_TYPE + ")";

    private static final String SQL_DELETE_CONTACTO = " DROP TABLE IF EXISTS " + DefinirTabla.Estado.TABLE_NAME;
    public static final String DATABASE_NAME = "agenda.db";
    public static final int DATABASE_VERSION = 1;

    public EstadoDbHelper(Context context) {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_CONTACTO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(SQL_DELETE_CONTACTO);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion){
        onUpgrade(db, oldVersion, newVersion);
    }
}
